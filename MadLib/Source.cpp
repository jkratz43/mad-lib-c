//MadLib: One Crazy Wedding!
//Jonathan Kratz
//May 29, 2015

#include <iostream>
#include <string>
using namespace std;

void main() {
	char a[100];
	char b[100];
	char c[100];
	char d[100];
	char e[100];
	char f[100];
	char g[100];
	char h[100];
	char i[100];
	char j[100];
	char k[100];
	char l[100];
	char m[100];
	char n[100];
	char o[100];
	char p[100];
	char q[100];
	char r[100];
	char s[100];
	char t[100];
	char u[100];
	char v[100];
	char w[100];
	char x[100];
	char y[100];
	char z[100];
	char aa[100];
	char bb[100];
	int exitPrg;
	cout << "MadLib: One Crazy Wedding!" << endl << endl;
	cout << "Provide words of the indicated type when prompted to make the story." << endl << endl;
	cout << "Duration of Time: " << flush;
	cin.getline(a, sizeof(a));
	cout << endl;
	cout << "Name of Bride: " << flush;
	cin.getline(b, sizeof(b));
	cout << endl;
	cout << "Name of Groom: " << flush;
	cin.getline(c, sizeof(c));
	cout << endl;
	cout << "Adjective: " << flush;
	cin.getline(d, sizeof(d));
	cout << endl;
	cout << "Clothing: " << flush;
	cin.getline(e, sizeof(e));
	cout << endl;
	cout << "Adjective: " << flush;
	cin.getline(f, sizeof(f));
	cout << endl;
	cout << "Something you wear: " << flush;
	cin.getline(g, sizeof(g));
	cout << endl;
	cout << "Adjective: " << flush;
	cin.getline(h, sizeof(h));
	cout << endl;
	cout << "Clothing: " << flush;
	cin.getline(i, sizeof(i));
	cout << endl;
	cout << "Verb past tense (ending in -ed): " << flush;
	cin.getline(j, sizeof(j));
	cout << endl;
	cout << "Verb past tense (ending in -ed): " << flush;
	cin.getline(k, sizeof(k));
	cout << endl;
	cout << "Adjective: " << flush;
	cin.getline(l, sizeof(l));
	cout << endl;
	cout << "Noun: " << flush;
	cin.getline(m, sizeof(m));
	cout << endl;
	cout << "Profession/Job: " << flush;
	cin.getline(n, sizeof(n));
	cout << endl;
	cout << "Verb: " << flush;
	cin.getline(o, sizeof(o));
	cout << endl;
	cout << "Noun Plural: " << flush;
	cin.getline(p, sizeof(p));
	cout << endl;
	cout << "Verb past tense (ending in -ed): " << flush;
	cin.getline(q, sizeof(q));
	cout << endl;
	cout << "Verb past tense (ending in -ed): " << flush;
	cin.getline(r, sizeof(r));
	cout << endl;
	cout << "A Food: " << flush;
	cin.getline(s, sizeof(s));
	cout << endl;
	cout << "Name of a Dance: " << flush;
	cin.getline(t, sizeof(t));
	cout << endl;
	cout << "Noun: " << flush;
	cin.getline(u, sizeof(u));
	cout << endl;
	cout << "Verb past tense (ending in -ed): " << flush;
	cin.getline(v, sizeof(v));
	cout << endl;
	cout << "Body Part Plural: " << flush;
	cin.getline(w, sizeof(w));
	cout << endl;
	cout << "Verb past tense (ending in -ed): " << flush;
	cin.getline(x, sizeof(x));
	cout << endl;
	cout << "Phrase: " << flush;
	cin.getline(y, sizeof(y));
	cout << endl;
	cout << "Adjective: " << flush;
	cin.getline(z, sizeof(z));
	cout << endl;
	cout << "Verb: " << flush;
	cin.getline(aa, sizeof(aa));
	cout << endl;
	cout << "Noun plural: " << flush;
	cin.getline(bb, sizeof(bb));
	cout << endl;
	cout << endl;
	cout << "After " << a << " of engagement, " << b << " and " << c << " had finally arrived at their big day!" << endl
		<< "The bride wore a " << d << " " << e << " and some " << f << " " << g << " while the groom wore a " << endl
		<< h << " " << i << ". The brides father " << j << " her down the aisle. Right as the preacher began" << endl
		<< "to speak, the best man " << k << " and the preacher consumed by the " << l << " " << m << " passed out. Luckily " << endl
		<< "the bride was a " << n << " so she was able to " << o << " the preacher. It wasn't long after that they " << endl
		<< "exchanged their " << p << ", " << q << ", and were pronounced " << r << ". Later at the reception " << s << endl
		<< "was served and everybody did a dance called " << t << " where they all sat on the " << u << " and " << v << endl
		<< "their " << w << ". It was quite a sight to see. The best part was when the groom " << x << " the bride " << endl
		<< "during the first dance and said " << "'" << y << "'" << ". How " << z << ". No doubt these two will be " << endl
		<< "together forever, bound to " << aa << " each others " << bb << "." << endl;
	cout << endl;
	cin >> exitPrg;
	// end program
	return;
}